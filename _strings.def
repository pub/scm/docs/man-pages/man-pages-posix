.\" Copyright 2016, The Open Group
.ds SI %Z% %I% %E%
.ds XO Open Group Standard
.ds XQ \*(XO (2018)
.ds XP Base Specifications, Issue 7
.ds Xp
.ds XZ \*(DT
.\"
.\" Issue 7 References
.\"
.ds zA POSIX.1\(hy2008
.ds qZ IEEE\ Std\ 1003.1\(hy2001
.\"
.ds Zz Base Definitions volume of POSIX.1\(hy2017
.ds Zy System Interfaces volume of POSIX.1\(hy2017
.ds Zx Shell and Utilities volume of POSIX.1\(hy2017
.ds zr Shell and Utilities volume of POSIX.1
.ds zj Rationale (Informative) volume of POSIX.1\(hy2017
.ds zo IEEE\ Std\ 1003.1\(hy2001/Cor\ 1\(hy2002
.ds zp IEEE\ Std\ 1003.1\(hy2001/Cor\ 2\(hy2004
.\"
.ds z9 This volume of POSIX.1\(hy2017
.ds Z` this volume of POSIX.1\(hy2017
.\"
.\" Standards docs
.\"
.ds Z1 ANSI\ C standard
.ds Z3 ISO\ C standard
.\"
.ds Z5 ISO\ POSIX\(hy1 standard
.ds Z2 ISO\ POSIX\(hy1:\|1996 standard
.ds Z% POSIX.1\(hy1988 standard
.ds zC POSIX.1\(hy1990 standard
.ds zB IEEE\ Std\ 1003.1\(hy1988
.\"
.ds zJ IEEE\ P1003.1a draft standard
.ds zE IEEE\ P1003.1c draft standard
.ds ZB IEEE\ Std 1003.1g\(hy2000
.ds Z- IEEE\ Std 1003.1d\(hy1999
.ds z+ IEEE\ Std 1003.1j\(hy2000
.ds za IEEE\ Std 1003.1q\(hy2000
.ds ZL IEEE\ Std 1003.2\(hy1992
.ds ZM IEEE\ P1003.2b draft standard
.ds zd IEEE\ Std\ 1003.13\(hy1998
.ds ze IEEE\ Std\ 1003.9\(hy1992
.ds zf IEEE\ Std\ 1003.0\(hy1995
.ds z_ IEEE\ Std\ 1003.2d\(hy1994
.ds Z@ IEEE\ Std\ 754\(hy1985
.ds z[ IEEE\ Std\ 854\(hy1987
.\"
.ds z0 ISO\ POSIX\(hy2 standard
.ds ZK ISO\ POSIX\(hy2:\|1993 standard
.ds Z9 ISO/IEC\ 9945\(hy2:\|1993 standard
.ds zD ISO/IEC\ 9945\(hy2 standard
.ds zI ISO/IEC\ 1539:\|1991 standard
.ds Z& ISO\ 4217:\|2001 standard
.ds z@ ISO/IEC\ 4873:\|1991 standard
.ds Z8 ISO/IEC\ 646:\|1991 standard
.ds Z6 ISO/IEC\ 6937:\|2001 standard
.ds Z0 ISO\ 8601:\|2004 standard
.ds zG ISO/IEC\ 8802\(hy3:\|1996 standard
.ds Z7 ISO/IEC\ 8859\(hy1:\|1998 standard
.ds Z] ISO/IEC\ 9899:\|1990/Amendment 1:\|1995 (E)
.ds z] ISO/IEC\ 9899:\|1990 standard
.ds Z? ISO/IEC\ 9899:\|1999 standard
.ds ZN ISO/IEC\ 10646\(hy1:\|2000 standard
.ds Z} ISO\ 2375:\|1985 standard
.ds Zk ISO/IEC\ 6429:\|1992 standard
.ds Z| IEC\ 60559:\|1989 standard
.\"
.ds z7 ANSI\ X3.9\(hy1978 standard
.ds z8 MSE working draft
.\"
.ds zF 1984 /usr/group standard
.ds Rt POSIX Realtime Extension
.ds Th POSIX Threads Extension
.ds z% FIPS 151\(hy2
.ds z" FIPS 151\(hy1
.\"
.\" Committees
.\"
.ds ZX IEEE PASC 1003.2 Interpretations Committee
.ds ZY IEEE PASC Shell and Utilities Working Group
.\"
.\" Misc docs
.\"
.ds zH referenced Sarwate article
.ds z3 referenced \f2The AWK Programming Language\fP
.ds q1 referenced \f2The C Programming Language\fP
.ds z4 referenced DeRemer and Pennello article
.ds z5 Knuth article
.ds z6 referenced \f2Yacc: Yet Another Compiler-Compiler\fP
.ds zM HP\(hyUX Manual
.ds CZ referenced UNIX Programmer's Manual
.ds DZ referenced Almasi and Gottlieb (1989)
.\"
.\" TOG docs
.\"
.ds BZ System Interfaces and Headers, Issue 5 specification
.ds ZI Internationalization Guide
.ds Z4 XNS, Issue 4 specification
.ds ZS XNS, Issue 5 specification
.ds z2 XNS, Issue 5.2 specification
.ds z& Headers Interface, Issue 3 specification
.ds Z, X/Open Curses, Issue 4, Version 2
.\"
.\" Revised part page for Base I7:
.de Tl
.ds XT \\*(XO
.ds XC
.nr Xz 1
.nr xF 0
\&
.af % 1
.bp \\$1
.ie !'\\$2'' .nr xo$Tl \\$1
.el .nr xo$Tl 0
.ie \\n[xo$Tl]=1 \{\
.OF
.\}
.el .OF "!\f1\s8\\\\*(XP \\\\*(Xp!Copyright \(co 2001-2018, IEEE and The Open Group. All rights reserved.!%\fP\s11!"
.fP
.if \\nX=0 .Nm +0
.S +3
.B
\\*(XP
.sp 0.3c
\\*(Xp
.sp 2.2c
.tm CONT\\t\\s+3\\f3\\*(XV\\t\\*(Xp\\s0\\t\\n%\\f1
.I
.S -3
The Open Group
.br
The Institute of Electrical and Electronics Engineers, Inc.
.ft 1
.EF "!\f1\s8%!Copyright \(co 2001-2018, IEEE and The Open Group. All rights reserved.\fP!\\\\*(XP \\\\*(Xp\s11!"
.ds XT
.bp
.OF "!\f1\s8\\\\*(XP \\\\*(Xp!Copyright \(co 2001-2018, IEEE and The Open Group. All rights reserved.!%\fP\s11!"
..
.\"
.ie \nX==0 .ds XR \\*(XQ
.el .ds XR \\*(XQ
.\"
.EF "!\f1\s8%!Copyright \(co 2001-2018, IEEE and The Open Group. All rights reserved.\fP!\\\\*(XP \\\\*(Xp\s11!"
.OF "!\f1\s8\\\\*(XP \\\\*(Xp!Copyright \(co 2001-2018, IEEE and The Open Group. All rights reserved.!%\fP\s11!"
.\"
